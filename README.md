Takes a mongo-like query object and compiles it into an SQL statement. Supports the majority of mongo query operator syntax (described below).

#Examples

```
#!javascript
// SELECT * FROM `some_table`

{
	$select: true,
	$columns: {'*': true},
	$table: 'some_table'
}
```

```
#!javascript
// SELECT `columnA`, `columnB`, * FROM `some_table`

{
	$select: true,
	$columns: {
		'columnA': true,
		'columnB': true,
		'*': true
	},
	$table: 'some_table'
}
```

```
#!javascript
// SELECT COUNT(*) FROM `some_table`

{
	$select: true,
	$columns: {'COUNT(*)': {$verbatim: true}},
	$table: 'some_table'
}
```


```
#!javascript
// SELECT COUNT(*) AS `count` FROM `some_table`

{
	$select: true,
	$columns: {'COUNT(*)': {$verbatim: 'count'}},
	$table: 'some_table'
}
```


```
#!javascript
// SELECT * FROM `some_table` WHERE `columnA` = "1" AND (`columnB` = "2" AND `columnC` = "3") AND (`columnD` = "4" OR `columnE` = "5")
{
	$select: [
		{columnA: 1},
		{$and: [
			{columnB: 2},
			{columnC: 3}
		]},
		{$or: [
			{columnD: 4},
			{columnE: 5}
		]}
	],
	$columns: {'*': true},
	$table: 'some_table'
}
```


```
#!javascript
// SELECT * FROM `some_table` WHERE `columnA` = "1" AND (`columnB` = "2" OR (`columnC` = "3" AND (`columnD` = "4")))

{
	$select: [
		{columnA: 1},
		{$or: [
			{columnB: 2},
			{$and: [
				{columnC: 3},
				{$or: [
					{columnD: 4}
				]}
			]}
		]},
	],
	$columns: {'*': true},
	$table: 'some_table'
}
```


```
#!javascript
// SELECT * FROM `some_table` WHERE `columnA` = "1" AND `columnB` != "2" AND `columnC` > "3" AND `columnD` >= "4" AND `columnE` < "5" AND `columnF` <= "6" AND `columnG` IN("7", "8", "9") AND `columnH` NOT IN("10", "11", "12") AND (`columnI` >= "13" AND `columnI` < "14")

{
	$select: [
		{columnA: 1},
		{columnB: {$ne: 2}},
		{columnC: {$gt: 3}},
		{columnD: {$gte: 4}},
		{columnE: {$lt: 5}},
		{columnF: {$lte: 6}},
		{columnG: {$in: [7, 8, 9]}},
		{columnH: {$nin: [10, 11, 12]}},
		{columnI: {$between: [13, 14]}}
	],
	$columns: {'*': true},
	$table: 'some_table'
}
```

# Top-Level Keys

* $insert, $select, $update, $delete: Designates the primary operation of this query.
    * Values: true || Array of conditions to excavate (see examples above)
    * At the time of this writing, only $select is implemented since that's all my project needs.
* $limit: Limits how many rows are returned
    * `{$limit: 1} // LIMIT 1`
    * `{$limit: '1, 2'} // LIMIT 1, 2`
    * `{$limit: [1, 2]} // LIMIT 1, 2` 
* $orderBy: Organized retrieved rows
    * ```{$orderBy: {columnA: 1, columnB: -1}} // ORDER BY `columnA` ASC, `columnB` DESC```
* $table: Sets the table being accessed
    * `{$table: 'some_table'}`
* $groupBy
    * NOT IMPLEMENTED (again, my project doesn't need it yet)

# Parity with Mongo Operator Syntax

### Non-Mongo ###
* $verbatim: Inserts supplied string directly into SQL
    * `{'COUNT(*)': {$verbatim: true}} // COUNT(*)`

### Supported ###
* $and
* $or
* $gt
* $gte
* $in
* $lt
* $lte
* $ne
* $nin
* $exists
    * as $isNotNull

### Workaround-able ###
* $mod
    * as ```{"MOD(X, Y)": {$verbatim: false}}```
* $regex
    * as ```{"`column` REGEX regex": {$verbatim: false}}```

### Not Supported ###
* $nor
* $not

### Unlikely to ever support ###
* $type
* $where 
* $geoIntersects
* $geoWithin
* $nearSphere
* $near
* $all
* $elemMath
* $size
* $comment
* $
* $elemMatch
* $meta
* $slice