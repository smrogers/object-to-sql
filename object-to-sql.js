define(function (require) {
	'use strict';

	// libraries
	var _ = require('underscore');

	function ObjectToSQLParser() {}
	ObjectToSQLParser.prototype = {

/*******************************************************************************
* PUBLIC METHODS
*******************************************************************************/

		/**
		 * Takes a mongo-like query object, processing it, and returns a
		 * compiled SQL string.
		 * @param  {object} uncompiled An uncompiled query object
		 * @return {string}            A generated SQL string
		 */
		convert: function (uncompiled) {
			var query = {};

			_.each(uncompiled || {}, function (value, key) {
				key = '_' + _.trim(key || '').toLowerCase();

				if (!_.isFunction(this[key])) {
					console.warn('No method named: %s', key);
					return;
				}

				this[key](query, value);
			}, this);

			var sql = this._compile(query, uncompiled);

			return sql;
		},

/*******************************************************************************
* PRIVATE METHODS
*******************************************************************************/

		/**
		 * Takes a compiled query object and generates a SQL string.
		 * @param  {object} query The compiled query object
		 * @return {string}       The generated SQL string
		 */
		_compile: function (query) {
			var format;
			var variables = [];
			switch (query.type) {
			case 'insert':
				console.warn('INSERT NOT IMPLEMENTED!');
				break;
			case 'select':
				format = 'SELECT %s FROM %s %s %s %s';
				variables.push(query.columns || '');
				variables.push(query.table || '');
				variables.push(query.conditions || '');
				variables.push(query.orderby || '');
				variables.push(query.limit || '');
				break;
			case 'update':
				console.warn('UPDATE NOT IMPLEMENTED!');
				break;
			case 'delete':
				console.warn('DELETE NOT IMPLEMENTED!');
				break;
			}

			//
			variables.unshift(format);
			var sql = _.sprintf.apply(_, variables);

			//
			return sql;
		},

/*******************************************************************************
* QUERY TYPE METHODS
*
* I'm wigging out that these are all the same length... So much so I'm not
* writing up the jsdoc headers on them.
*******************************************************************************/

		_$insert: function (query, conditions) { // C
			query.type = 'insert';
			this._$conditions(query, conditions);
		},

		_$select: function (query, conditions) { // R
			query.type = 'select';
			this._$conditions(query, conditions);
		},

		_$update: function (query, conditions) { // U
			query.type = 'update';
			this._$conditions(query, conditions);
		},

		_$delete: function (query, conditions) { // D
			query.type = 'delete';
			this._$conditions(query, conditions);
		},

/*******************************************************************************
* QUERY CONDITIONS
*******************************************************************************/

		/**
		 * Excavate recursively a conditions object for SQL expressions.
		 * @param  {object} query     The compiled query object
		 * @param  {array} conditions The top-level set of conditions
		 *
		 * Example:
		 * [
		 * 	{testA: 1},
		 * 	{$or: [
		 * 		testB: 2,
		 * 		testC: {$ne: 3},
		 * 		{$and: [
		 * 			testD: {$isNotNull: true},
		 * 			testE: {$gt: 4},
		 * 		]}
		 * 	]}
		 * ]
		 *
		 * WHERE `testA` = 1 AND (`testB` = 2 OR `testC` != 3 OR (`testD` IS
		 * NOT NULL AND `testE` > 4))
		 */
		_$conditions: function (query, conditions) {
			if (_.isEmpty(conditions)) { return; }
			if (conditions === true) { return; }

			if (!_.isArray(conditions)) {
				console.error('top level conditions must be array: %o', conditions);
				throw new Error('top level conditions must be array!');
			}

			//
			var processed = [];
			_.each(conditions, function (value) {
				processed.push(this._column(value));
			}, this);

			// top level conditions are always AND
			processed = processed.join(' AND ');

			//
			if (!processed.length) { return; }
			query.conditions = 'WHERE ' + processed;
		},

		/**
		 * Does the recursive heavy-lifting, mapping column objects to methods
		 * @param  {obj} column A object to excavate
		 * @return {arr}        The processed contents
		 */
		_column: function (column) {
			if (!_.isObject(column)) {
				console.error('condition column must be object: %o', column);
				throw new Error('condition column must be array!');
			}

			//
			var processed = [];
			_.each(column, function (value, key) {
				// $and, $nor, $not, $or
				var fkey = '_' + _.trim(key || '').toLowerCase();
				if (_.isFunction(this[fkey])) {
					return processed.push(this[fkey](value));
				}

				// {key: {$key: true}}
				// $gt, $lte, etc
				var isObject = _.isObject(value);
				if (isObject) {
					return _.each(value, function(innerValue, innerKey) {
						var innerFKey = '_' + _.trim(innerKey || '').toLowerCase();
						if (!_.isFunction(this[innerFKey])) {
							console.warn('No method named: %s %o', innerKey, value);
						}

						processed.push(this[innerFKey](key, innerValue));
					}, this);
				}

				// regular column = value
				processed.push(this._$e(key, value, '='));
			}, this);

			return processed;
		},

		/**
		 * Recursively compiles a series of AND expressions.
		 *
		 * @param  {array} arr The expression to compile
		 * @return {string}    The compiled expressioned partitioned with AND
		 *
		 * Example:
		 * {$and: [{testA: 1, testB: 2}]}
		 * (`testA` = 1 AND `testB` = 2)
		 */
		_$and: function (arr) {
			if (!_.isArray(arr)) {
				console.error('arr must be array! %o', arr);
				throw new Error('arr must be array!');
			}

			//
			var processed = [];
			_.each(arr, function (value) {
				processed.push(this._column(value));
			}, this);

			// wrap in ()
			processed = '(' + processed.join(' AND ') + ')';

			//
			return processed;
		},

		/**
		 * Recursively compiles a series of OR expressions.
		 *
		 * @param  {array} arr The expression to compile
		 * @return {string}    The compiled expressioned partitioned with OR
		 *
		 * Example:
		 * {$or: [{testA: 1, testB: 2}]}
		 * (`testA` = 1 OR `testB` = 2)
		 */
		_$or: function (arr) {
			if (!_.isArray(arr)) {
				console.error('arr must be array! %o', arr);
				throw new Error('arr must be array! %o', arr);
			}

			//
			var processed = [];
			_.each(arr, function (value) {
				processed.push(this._column(value));
			}, this);

			//
			processed = '(' + processed.join(' OR ') + ')';

			return processed;
		},

		/**
		 * Creates a basic X = Y expresion. The = is configurable.
		 *
		 * @param  {string} key      the table column
		 * @param  {string} value    the testing value
		 * @param  {string} operator the logic operator (>, <, etc)
		 * @return {string}          the generated expression
		 */
		_$e: function (key, value, operator) {
			operator = operator || '=';
			return '`' + key + '` ' + operator + ' "' + value + '"';
		},

		/**
		 * Create a basic X != Y expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {string} value the testing value
		 * @return {string}       the generated expression
		 */
		_$ne: function (key, value) {
			return this._$e(key, value, '!=');
		},

		/**
		 * Create a basic X > Y expression.
		 *
		 * @param  {string} key      the table column
		 * @param  {string} value    the testing value
		 * @return {string}          the generated expression
		 */
		_$gt: function (key, value) {
			return this._$e(key, value, '>');
		},

		/**
		 * Create a basic X >= Y expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {string} value the testing value
		 * @return {string}       the generated expression
		 */
		_$gte: function (key, value) {
			return this._$e(key, value, '>=');
		},

		/**
		 * Create a basic X < Y expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {string} value the testing value
		 * @return {string}       the generated expression
		 */
		_$lt: function (key, value) {
			return this._$e(key, value, '<');
		},

		/**
		 * Create a basic X <= Y expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {string} value the testing value
		 * @return {string}       the generated expression
		 */
		_$lte: function (key, value) {
			return this._$e(key, value, '<=');
		},

		/**
		 * Create a basic X IS NOT NULL expression.
		 *
		 * @param  {string} key the table column
		 * @return {string}     the generated expression
		 */
		_$isnotnull: function (key) {
			return '`' + key + '` IS NOT NULL';
		},

		/**
		 * Create a basic X IS NULL expression.
		 *
		 * @param  {string} key the table column
		 * @return {string}     the generated expression
		 */
		_$isnull: function (key) {
			return '`' + key + '` IS NULL';
		},

		/**
		 * Spits out whatever key is "verbatim", optionally wrapping in ().
		 * @param  {string} key  string to spit
		 * @param  {bool}   wrap Do we wrap?
		 * @return {string}      the generated expression
		 *
		 * Example:
		 * {'SELECT MAX(id) FROM `otherTable`': {$verbatim: true}}
		 * (SELECT MAX(id) FROM `otherTable`)
		 */
		_$verbatim: function (key, wrap) {
			return (wrap) ? '(' + key + ')' : key;
		},

		/**
		 * Create a basic X IN(Y, ...) expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {array}  value the testing value
		 * @return {string}       the generated expression
		 */
		_$in: function (key, value) {
			if (!_.isArray(value)) {
				console.error('value must be array! %s %o', key, value);
				throw new Error('value must be array!');
			}

			return '`' + key + '` IN("' + value.join('", "') + '")';
		},

		/**
		 * Create a basic X NOT IN(Y, ...) expression.
		 *
		 * @param  {string} key   the table column
		 * @param  {array}  value the testing value
		 * @return {string}       the generated expression
		 */
		_$nin: function (key, value) {
			if (!_.isArray(value)) {
				console.error('value must be array! %s %o', key, value);
				throw new Error('value must be array!');
			}

			return '`' + key + '` NOT IN("' + value.join('", "') + '")';
		},

		/**
		 * Returns an expression where key is >= AND < value.
		 *
		 * < not <=
		 * < not <=
		 * < not <=
		 *
		 * @param  {string} key  The column in question
		 * @param  {array} value [valueA, valueB]
		 * @return {string}      The generated expression
		 */
		_$between: function(key, value) {
			if (!_.isArray(value)) {
				console.error('value must be array! %s %o', key, value);
				throw new Error('value must be array!');
			}

			if (value.length < 2) {
				console.error('$between has too few columns: %s %o', key, value);
				throw new Error('$between has too few columns!');
			}

			if (value.length > 2) {
				console.error('$between has too many columns: %s %o', key, value);
				throw new Error('$between has too many columns!');
			}

			return '(`' + key + '` >= "' + value[0] + '" AND `' + key + '` < "' + value[1] + '")';
		},

/*******************************************************************************
* QUERY MISCELLANEOUS METHODS
*******************************************************************************/

		/**
		 * Sets the table for a query.
		 * @param  {object} query      The query compile object
		 * @param  {string} tableName  The table name
		 *
		 * Example:
		 * $table: tableName
		 */
		_$table: function (query, tableName) {
			query.table = '`' + tableName + '`';
		},

		/**
		 * Configure the columns involved in this query
		 * @param  {object} query   The query compile object
		 * @param  {object} columns The columns involved
		 */
		_$columns: function (query, columns) {
			var processed = [];
			_.each(columns || {}, function (value, key) {
				// {'*': true}
				// *
				if (key === '*') {
					return processed.push('*');
				}

				// {'COUNT(*)': {$verbatim: true}}
				// COUNT(*)
				var isObject = _.isObject(value);
				if (isObject && value.$verbatim === true) {
					return processed.push(key);

				// {'COUNT(*)': {$verbatim: 'value'}}
				// COUNT(*) AS `value`
				} else if (isObject) {
					return processed.push(key + ' AS `' + value.$verbatim + '`');

				// {'key': true}
				// `key`
				} else if (value === true) {
					return processed.push('`' + key + '`');

				// {'key': 'value'}
				// `key` AS `value`
				} else {
					return processed.push('`' + key + '` AS `' + value + '`');
				}
			}, this);

			//
			if (!processed) { return; }
			query.columns = processed.join(', ');
		},

		/**
		 * Configures the query in how to group rows
		 * @param  {object} query     The query compile object
		 * @param  {object} groupings [description]
		 * @return {[type]}            [description]
		 */
		_$groupby: function (query, groupings) {
			console.debug('$groupby', groupings);
			console.warn('Not Implemented!');
		},

		/**
		 * Configure the query's limit
		 * @param  {object} query The query compile object
		 * @param  {[type]} limit The limiting factors
		 *
		 * Examples:
		 * $limit: 1
		 * $limit: 'X, Y'
		 * $limit: [X, Y]
		 */
		_$limit: function (query, parameters) {
			if (!_.isArray(parameters)) {
				parameters = [parameters];
			}

			// there are only at most 2 limit parameters
			if (parameters.length > 2) {
				parameters = parameters.slice(0, 2);
			}

			//
			var factors = _.values(factors);
			if (!factors.length) { return; }

			//
			query.limit = 'LIMIT ' + factors.join(', ');
		},

		/**
		 * Configures the query to order by certain columns
		 * @param  {object} query   The query compile object
		 * @param  {object} columns how/what columns to sort
		 *
		 * Example:
		 * $orderBy: {columnA: 1, columnB: -1}
		 * $orderBy: {columnA: 1, $random: true}
		 */
		_$orderby: function (query, columns) {
			var processed = [];
			_.each(columns || {}, function (value, key) {
				if (_.trim(key || '').toLowerCase() === '$random') {
					return processed.push('RANDOM()');
				}

				if (value > 0) { // asc
					return processed.push('`' + key + '` ASC');
				} else { // desc
					return processed.push('`' + key + '` DESC');
				}
			});

			//
			if (!processed.length) { return; }
			query.orderby = 'ORDER BY ' + processed.join(', ');
		}

/*******************************************************************************
* /END
*******************************************************************************/
	};

	return new ObjectToSQLParser();
});
